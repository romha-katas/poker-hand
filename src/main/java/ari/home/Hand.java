package ari.home;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Hand implements Comparable<Hand> {
  private final List<Card> cards;

  public Hand(List<Card> cards) {
    if (cards.size() != 5) {
      throw new IllegalArgumentException("The hand should contain 5 cards");
    }
    this.cards = cards;
  }

  public Stream<Card> drawAll() {
    return cards.stream();
  }

  public Stream<Value> values() {
    return cards.stream().map(Card::value);
  }

  public Stream<Value> distinctValues() {
    return values().distinct().sorted();
  }
  public Stream<Suit> suits() {
    return cards.stream().map(Card::suit);
  }

  public Combination bestCombination() {
    return Arrays.stream(Rule.values()).map(it -> it.matches(this)).filter(Objects::nonNull)
        .reduce((first, second) -> second)
        .orElse(null); // Cannot happen since the rule HighCard is always applicable
  }

  @Override
  public int compareTo(Hand other) {
    return this.bestCombination().compareTo(other.bestCombination());
  }
}
