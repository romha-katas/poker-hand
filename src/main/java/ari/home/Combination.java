package ari.home;

import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Combination implements Comparable<Combination> {
  private final Rule rule;
  private final List<Card> cards;

  public Combination(Rule rule, List<Card> cards) {
    this.rule = rule;
    this.cards = cards;
  }

  @Override
  public int compareTo(Combination other) {
    int ruleComparison = this.rule.compareTo(other.rule);
    if (ruleComparison == 0) { // Found the same kind of combination
      return compareCardByCard(other); // The highest card will define the winner
    }
    return ruleComparison;
  }

  private int compareCardByCard(Combination other) {
    return zip(cards, other.cards).map(pair -> pair.left.compareTo(pair.right))
        .filter(i -> i != 0).findFirst()  // we're looking for the first different card
        .orElse(0); // Same card in our hand and the other
  }

  private Stream<Pair> zip(List<Card> left, List<Card> right) {
    return IntStream.range(0, Math.min(left.size(), right.size())).mapToObj(
        i -> new Pair(this.cards.get(i), right.get(i)));
  }

  public static class Pair {
    public final Card left;
    public final Card right;
    public Pair(Card left, Card right) {
      this.left = left;
      this.right = right;
    }
  }
}
