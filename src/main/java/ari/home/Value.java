package ari.home;

public enum Value {
  TWO,
  THREE,
  FOUR,
  FIVE,
  SIX,
  SEVEN,
  EIGHT,
  NINE,
  TEN,
  JACK,
  QUEEN,
  KING,
  ACE;

  Integer minus(Value rValue) {
    return (this.ordinal() + 1) - (rValue.ordinal() + 1);
  }
}
