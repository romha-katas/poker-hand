package ari.home;

public enum Suit {
  HEART,
  DIAMOND,
  CLUBS,
  SPADE
}
