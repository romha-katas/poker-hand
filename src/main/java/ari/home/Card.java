package ari.home;

public class Card implements Comparable<Card> {
  private final Value value;
  private final Suit suit;

  public Card(Value value, Suit suit) {
    this.value = value;
    this.suit = suit;
  }

  Value value() {
    return value;
  }
  public Suit suit() { return suit; }

  @Override
  public int compareTo(Card other) {
    return value.compareTo(other.value);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Card card = (Card) o;
    return value == card.value && suit == card.suit;
  }
}
