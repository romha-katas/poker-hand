package ari.home;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum Rule {
  HighestCard {
    @Override
    Combination matches(Hand hand) {
      List<Card> cards = hand.drawAll().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
      return new Combination(this, cards);
    }
  },
  Pair {
    @Override
    Combination matches(Hand hand) {
      List<List<Card>> groups = groupOfAtLeastIn(hand, 2);
      if (groups.isEmpty()) {
        return null;
      }
        return new Combination(this,
            mergeStreams(groups.get(0).stream(),
                         without(hand, groups.get(0))).collect(Collectors.toList()));
    }
  },
  TwoPair {
    @Override
    Combination matches(Hand hand) {
      List<List<Card>> groups = groupOfAtLeastIn(hand, 2);
      if (groups.size() < 2) {
        return null;
      }
      return new Combination(this,
            mergeStreams(groups.get(0).stream(),
                         groups.get(1).stream(),
                         without(hand, groups.get(0))).collect(Collectors.toList()));
    }
  },
  ThreeOfAKind {
    @Override
    Combination matches(Hand hand) {
      List<List<Card>> groups = groupOfAtLeastIn(hand, 3);
      if (groups.isEmpty()) {
        return null;
      }
      return new Combination(this, groups.get(0));
    }
  },
  Straight {
    @Override
    Combination matches(Hand hand) {
      List<Value> values = hand.values().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
      if (values.get(0).minus(values.get(values.size() - 1)) == 4) {
        return new Combination(this, hand.drawAll().sorted(Comparator.reverseOrder()).collect(Collectors.toList()));
      }
      return null;
    }
  },
  Flush {
    @Override
    Combination matches(Hand hand) {
      List<Card> cards = hand.drawAll().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
      if (cards.stream().map(Card::suit).distinct().count() == 1) {
        return new Combination(this, cards);
      }
      return null;
    }
  },
  FullHouse {
    @Override
    Combination matches(Hand hand) {
      List<Integer> groups = groupOfAtLeastIn(hand, 2).stream().map(List::size).collect(Collectors.toList());
      if (groups.contains(2) && groups.contains(3))
        return new Combination(this, hand.drawAll().collect(Collectors.toList()));
      return null;
    }
  },
  FourOfAKind {
    @Override
    Combination matches(Hand hand) {
      List<List<Card>> groups = groupOfAtLeastIn(hand, 4);
      if (groups.size() == 1)
        return new Combination(this, groups.get(0));
      return null;
    }
  },
  StraightFlush {
    @Override
    Combination matches(Hand hand) {
      List<Value> values = hand.distinctValues().collect(Collectors.toList());
      long suits = hand.suits().distinct().count();
      if (suits == 1 && values.get(values.size() - 1).minus(values.get(0)) == 4)
        return new Combination(this, hand.drawAll().collect(Collectors.toList()));
      return null;
    }
  };
  abstract Combination matches(Hand hand);

  List<List<Card>> groupOfAtLeastIn(Hand hand, int count) {
    return hand.drawAll().collect(Collectors.groupingBy(Card::value))
        .entrySet().stream().filter(entry -> entry.getValue().size() >= count).sorted(this::compareByCountThenValue)
        .map(Map.Entry::getKey)
        .map(value -> hand.drawAll().filter(c -> c.value().equals(value)).collect(Collectors.toList()))
        .collect(Collectors.toList());



  }

  private int compareByCountThenValue(Map.Entry<Value, List<Card>> a, Map.Entry<Value, List<Card>> b) {
    if (a.getValue().size() == b.getValue().size()) {
      return b.getKey().compareTo(a.getKey());
    }
    return b.getValue().size() - a.getValue().size();
  }

  static Stream<Card> without(Hand hand, List<Card> cardsToDiscard) {
    return hand.drawAll()
        .filter(card -> !cardsToDiscard.contains(card));
  }

  private static <T> Stream<T> mergeStreams(Stream<T>... streams) {
    return Arrays.stream(streams).reduce(
        Stream::concat
    ).get();
  }
}
