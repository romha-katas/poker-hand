import ari.home.Rule;

public class ThreeOfAKindTest extends BaseTests {
  @Override
  String hand() {
    return "♢J ♢K ♤J ♡J ♧2";
  }

  @Override
  String weakerHand() {
    return "♢3 ♧J ♧3 ♡3 ♧Q";
  }

  @Override
  String weakerCombination() {
    return "♢3 ♧J ♧3 ♡5 ♧5";
  }

  @Override
  ExpectedCombination combination() {
    return a(Rule.ThreeOfAKind, "♢J ♤J ♡J");
  }
}
