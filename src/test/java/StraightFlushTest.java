import ari.home.Rule;

public class StraightFlushTest extends BaseTests {
  @Override
  String hand() {
    return "♢10 ♢J ♢Q ♢K ♢A";
  }

  @Override
  String weakerHand() {
    return "♢2 ♢J ♤2 ♡2 ♧2";
  }

  @Override
  String weakerCombination() {
    return "♢7 ♢J ♤7 ♡7 ♧7";
  }

  @Override
  ExpectedCombination combination() {
    return a(Rule.StraightFlush, "♢10 ♢J ♢Q ♢K ♢A");
  }
}
