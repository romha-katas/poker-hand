import ari.home.Rule;

public class StraightTest extends BaseTests {
  @Override
  String hand() {
    return "♧A ♢10 ♢J ♡K ♤Q";
  }

  @Override
  String weakerHand() {
    return "♢7 ♢8 ♤9 ♡10 ♧J";
  }

  @Override
  String weakerCombination() {
    return "♢J ♢K ♤J ♡J ♧2";
  }

  @Override
  ExpectedCombination combination() {
    return a(Rule.Straight, "♧A ♡K ♤Q ♢J ♢10");
  }
}
