import ari.home.*;
import org.assertj.core.api.Assertions;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public abstract class TestUtils {
  ExpectedCombination a(Rule r, String c) {
    return new ExpectedCombination(r, c);
  }

  static class ExpectedCombination {
    final Rule rule;
    final List<Card> cards;

    ExpectedCombination(Rule rule, String cards) {
      this.rule = rule;
      this.cards = TestUtils.toCards(cards);
    }
  }

  protected HandHelper the(String cards) {
    return new HandHelper(cards);
  }

  static class HandHelper {
    Hand hand;

    HandHelper(String cards) {
      hand = toHand(cards);
    }

    HandHelper should_expose(ExpectedCombination combination) {
      Combination result = this.hand.bestCombination();

      Assertions.assertThat(result).isEqualByComparingTo(new Combination(combination.rule, combination.cards));

      return this;
    }

    HandHelper should_win_against(String s) {
      Hand other = toHand(s);

      Assertions.assertThat(hand.compareTo(other)).isGreaterThan(0);

      return this;
    }
  }

  static Hand toHand(String s) {
    return new Hand(toCards(s));
  }

  static List<Card> toCards(String s) {
    return Arrays.stream(s.split(" ")).map(TestUtils::toCard).collect(Collectors.toList());
  }

  static Card toCard(String s) {
    Suit suit = toSuit(s.substring(0, 1));
    Value value = toValue(s.substring(1));

    return new Card(value, suit);
  }

  private static Suit toSuit(String s) {
    switch(s) {
      case "♡": return Suit.HEART;
      case "♢": return Suit.DIAMOND;
      case "♧": return Suit.CLUBS;
      case "♤": return Suit.SPADE;
      default: throw new IllegalArgumentException(s + " is not a valid suit");
    }
  }

  private static Value toValue(String s) {
    switch(s) {
      case "2": return Value.TWO;
      case "3": return Value.THREE;
      case "4": return Value.FOUR;
      case "5": return Value.FIVE;
      case "6": return Value.SIX;
      case "7": return Value.SEVEN;
      case "8": return Value.EIGHT;
      case "9": return Value.NINE;
      case "10": return Value.TEN;
      case "J": return Value.JACK;
      case "Q": return Value.QUEEN;
      case "K": return Value.KING;
      case "A": return Value.ACE;
      default: throw new IllegalArgumentException(s + " is not a valid value");
    }
  }

}
