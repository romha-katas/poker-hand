import ari.home.Rule;

public class FlushTest extends BaseTests {
  @Override
  String hand() {
    return "♧2 ♧J ♧3 ♧5 ♧K";
  }

  @Override
  String weakerHand() {
    return "♢2 ♢3 ♤4 ♡5 ♧6";
  }

  @Override
  String weakerCombination() {
    return "♧A ♢10 ♢J ♡K ♤Q";
  }

  @Override
  ExpectedCombination combination() {
    return a(Rule.Flush, "♧K ♧J ♧5 ♧3 ♧2");
  }
}
