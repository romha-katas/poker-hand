import ari.home.Rule;

public class FullHouseTest extends BaseTests {
  @Override
  String hand() {
    return "♢7 ♤7 ♡7 ♧J ♢J";
  }

  @Override
  String weakerHand() {
    return "♧2 ♧J ♧3 ♧5 ♧Q";
  }

  @Override
  String weakerCombination() {
    return "♧2 ♧J ♧3 ♧5 ♧K";
  }

  @Override
  ExpectedCombination combination() {
    return a(Rule.FullHouse, "♢7 ♤7 ♡7 ♧J ♢J");
  }
}
