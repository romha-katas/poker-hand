import ari.home.Rule;

public class FourOfAKindTest extends BaseTests {
  @Override
  String hand() {
    return "♢7 ♢J ♤7 ♡7 ♧7";
  }

  @Override
  String weakerHand() {
    return "♢2 ♢J ♤2 ♡2 ♧2";
  }

  @Override
  String weakerCombination() {
    return "♧K ♧J ♧5 ♧3 ♧2";
  }

  @Override
  ExpectedCombination combination() {
    return a(Rule.FourOfAKind, "♢7 ♤7 ♡7 ♧7");
  }
}
