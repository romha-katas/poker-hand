import ari.home.Rule;

public class TwoPairCard extends BaseTests {
  String hand() { return "♢J ♢K ♤J ♡K ♧Q";}

  String weakerHand() {return "♢3 ♧5 ♧3 ♡5 ♧Q";}

  String weakerCombination() { return "♢J ♢K ♤J ♡4 ♧2"; }

  ExpectedCombination combination() {return a(Rule.TwoPair, "♢K ♡K ♤J ♢J");}
}



