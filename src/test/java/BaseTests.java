import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public abstract class BaseTests extends TestUtils {

  @Test
  @DisplayName("should find the combination")
  void test1() {
    the(hand()).should_expose(combination());
  }

  @Test
  @DisplayName("should win against a weaker hand")
  void test2() {
    the(hand()).should_win_against(weakerHand());
  }

  @Test
  @DisplayName("should win against a weaker combination")
  void test3() {
    the(hand()).should_win_against(weakerCombination());
  }


  abstract String hand();
  abstract String weakerHand();
  abstract String weakerCombination();
  abstract ExpectedCombination combination();

}
