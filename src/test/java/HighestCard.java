import ari.home.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class HighestCard extends TestUtils {
  @Test
  @DisplayName("should find the highest card")
  void test1() {
    the("♢A ♢J ♤3 ♡2 ♧K").should_expose(a(Rule.HighestCard, "♢A"));
  }

  @Test
  @DisplayName("should win by its higher rank")
  void test2() {
    the("♢A ♢J ♤3 ♡2 ♧K").should_win_against("♢5 ♢J ♤3 ♡2 ♧K");
  }

  @Test
  @DisplayName("should win by its second higher rank")
  void test3() {
    the("♢A ♢J ♤3 ♡2 ♧K").should_win_against("♢A ♢J ♤3 ♡2 ♧Q");
  }
}



