import ari.home.Rule;

public class PairCard extends BaseTests {
  String hand() { return "♢J ♢K ♤J ♡4 ♧2"; }

  String weakerHand() { return "♢3 ♧J ♧3 ♡6 ♧Q"; }

  String weakerCombination() { return "♢A ♢J ♤3 ♡2 ♧K"; }

  ExpectedCombination combination() {return a(Rule.Pair, "♢J ♤J");}
}



